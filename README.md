NxFAdmin
========

Web administration template for NxFilter.

Extract archive contents to a location under your base NxFilter installation.  

For instance on linux and your NxFilter files are located in /nxfilter, then something like "tar zxf nxfadmin-master.tar.gz -C /nxfilter/" will create /nxfilter/skins/nxfadmin containing the new GUI files.  

Edit /nxfilter/conf/cfg.properties and add "www_dir = skins/nxfadmin" at the bottom.  

Save the file and restart NxFilter to activate the new GUI.  

================================================================================================================

